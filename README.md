Disclaimer: This is still in very early development. This README might describe future features. 



---

# Pátek Show Controller

## What?

A Raspberry Pi powered device, that can control lights, sound and video using a simple user interface your grandma can understand. 

## Why?

Small theatre groups and similar gatherings of people usually want to control lights and play some sound, but they definitely don't have the budget to buy their own equipment, let alone use it, because they don't have a dedicated “sound guy” and the technician is usually the actor who currently isn't on the stage. That's why we wanted to create a device even these small groups can afford and simple enough that any member of the squad can control it. 

## How? (a.k.a. the technical stuff)

The heart, soul and brain of our device is the Raspberry Pi, as it already has quite enough features, that we need. We can use its sound output, but adding a cheap USB sound card can do a lot to make it better. For controlling lights we have to somehow implement DMX, which we found is really easy using an Arduino Nano and the DmxSimple library. 

We have a docker container, that is ready to run on your RPi. Included is our web app, which contains the true selling point of our device - the script system.

## The script system

There are two phases to using , preparation and then execution. During preparation, you paste or write the script of your play into the *markdown-supported* text area. Next step is to replace your technical notes (e. g. Turn on the light, play Track 13 etc.) with clickable buttons using a dropdown and you‘re done. 

Then you switch to execution mode and your script turns into a rich-text version of the script, that is looks like what the actors are used to, with simple clickable buttons for the technical stuff.  

## Plug-and-play

You have a box. You plug in all the cables you have. It works. (Hopefully)

---

## Backstory

This was our project during the AT&T Hackaton 2019 in Brno, Czech Republic, which we decided to continue, because it seemed like a good idea. 