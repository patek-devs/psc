const PSCTemplate = document.createElement("template");
PSCTemplate.innerHTML = `
<style>
svg {
  height: 1em;
  padding-right: 1em;
}

:host {
  display: block;
}

.tag {
  align-items: center;
  background-color: whitesmoke;
  border-radius: 4px;
  color: #4a4a4a;
  display: inline-flex;
  font-size: 0.75rem;
  height: 2em;
  justify-content: center;
  line-height: 1.5;
  padding-left: 0.75em;
  padding-right: 0.75em;
  white-space: nowrap;
}

.tag.is-white {
  background-color: white;
  color: #0a0a0a;
}

.tag.is-black {
  background-color: #0a0a0a;
  color: white;
}

.tag.is-light {
  background-color: whitesmoke;
  color: rgba(0, 0, 0, 0.7);
}

.tag.is-dark {
  background-color: #363636;
  color: #fff;
}

.tag.is-primary {
  background-color: #00d1b2;
  color: #fff;
}

.tag.is-primary.is-light {
  background-color: #ebfffc;
  color: #00947e;
}

.tag.is-link {
  background-color: #3273dc;
  color: #fff;
}

.tag.is-link.is-light {
  background-color: #eef3fc;
  color: #2160c4;
}

.tag.is-info {
  background-color: #3298dc;
  color: #fff;
}

.tag.is-info.is-light {
  background-color: #eef6fc;
  color: #1d72aa;
}

.tag.is-success {
  background-color: #48c774;
  color: #fff;
}

.tag.is-success.is-light {
  background-color: #effaf3;
  color: #257942;
}

.tag.is-warning {
  background-color: #ffdd57;
  color: rgba(0, 0, 0, 0.7);
}

.tag.is-warning.is-light {
  background-color: #fffbeb;
  color: #947600;
}

.tag.is-danger {
  background-color: #f14668;
  color: #fff;
}

.tag.is-danger.is-light {
  background-color: #feecf0;
  color: #cc0f35;
}

.tag.is-normal {
  font-size: 0.75rem;
}

.tag.is-medium {
  font-size: 1rem;
}

.tag.is-large {
  font-size: 1.25rem;
}

.tag .icon:first-child:not(:last-child) {
  margin-left: -0.375em;
  margin-right: 0.1875em;
}

.tag .icon:last-child:not(:first-child) {
  margin-left: 0.1875em;
  margin-right: -0.375em;
}

.tag .icon:first-child:last-child {
  margin-left: -0.375em;
  margin-right: -0.375em;
}

.tag:hover {
  text-decoration: underline;
  cursor: pointer;
}
</style>
`;
const PSCBlockTemplate = document.createElement("template");
PSCBlockTemplate.innerHTML = `
<style>
	:host {
		display: block;
	}
	div#wrapper {
		border: solid 3px orangered;
		border-radius: 3px;
		padding: 5px;
		background-color: orange;
		position: relative;
	}
	#buttons {
		display: flex;
		flex-wrap: wrap;
		flex-direction: row;
		justify-content: flex-start;
		align-items: flex-start;
		align-content: flex-start;
	}
	#buttons::slotted(*) {
		margin: 5px;
		z-index: 1;
	}
	div#clicker {
		z-index: 3;
		position: absolute;
		width: 100%;
		height: 100%;
		border-radius: 3px;
		transition-duration: 0.4s;
		top: 0;
		left: 0;
		background-color: #ffa50066;
		opacity: 0;

	}
	div#clicker:hover {
		cursor: pointer;
		opacity: 1;
	}
</style>
<div id="wrapper">
	<span id="running" style="display: none">Running | </span><span id="params"></span>
	<div id="clicker">
		
	</div>
	<slot id="buttons">
		
	</slot>
</div>
`;
const PSCCallTemplate = document.createElement("template");
PSCCallTemplate.innerHTML = `
<style>
:host {
  display: block;
}
#tag {
  align-items: center;
  background-color: #6d57c6;
  border-radius: 4px;
  color: white;
  display: inline-flex;
  font-size: 0.75rem;
  height: 2em;
  justify-content: center;
  line-height: 1.5;
  padding-left: 0.75em;
  padding-right: 0.75em;
  white-space: nowrap;
}
#tag::before {
	content: "f()";
	font-size: large;
	margin-right: 0.5em;
	font-family: monospace;
	font-weight: bold;
}

#tag:hover {
  text-decoration: underline;
  cursor: pointer;
}
</style>
<div id="tag">
	
</div>
`;
const modules = {
	audio: {
		play: {
			icon: "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"music\" class=\"svg-inline--fa fa-music fa-w-16\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 512 512\"><path fill=\"currentColor\" d=\"M470.38 1.51L150.41 96A32 32 0 0 0 128 126.51v261.41A139 139 0 0 0 96 384c-53 0-96 28.66-96 64s43 64 96 64 96-28.66 96-64V214.32l256-75v184.61a138.4 138.4 0 0 0-32-3.93c-53 0-96 28.66-96 64s43 64 96 64 96-28.65 96-64V32a32 32 0 0 0-41.62-30.49z\"></path></svg>",
			color: "success",
			text: "Play: {displayName}"
		},
		stop: {
			icon: "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"stop\" class=\"svg-inline--fa fa-stop fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"currentColor\" d=\"M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48z\"></path></svg>",
			color: "danger",
			text: "Stop audio"
		},
		volume: {
			icon: "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"volume-up\" class=\"svg-inline--fa fa-volume-up fa-w-18\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 576 512\"><path fill=\"currentColor\" d=\"M215.03 71.05L126.06 160H24c-13.26 0-24 10.74-24 24v144c0 13.25 10.74 24 24 24h102.06l88.97 88.95c15.03 15.03 40.97 4.47 40.97-16.97V88.02c0-21.46-25.96-31.98-40.97-16.97zm233.32-51.08c-11.17-7.33-26.18-4.24-33.51 6.95-7.34 11.17-4.22 26.18 6.95 33.51 66.27 43.49 105.82 116.6 105.82 195.58 0 78.98-39.55 152.09-105.82 195.58-11.17 7.32-14.29 22.34-6.95 33.5 7.04 10.71 21.93 14.56 33.51 6.95C528.27 439.58 576 351.33 576 256S528.27 72.43 448.35 19.97zM480 256c0-63.53-32.06-121.94-85.77-156.24-11.19-7.14-26.03-3.82-33.12 7.46s-3.78 26.21 7.41 33.36C408.27 165.97 432 209.11 432 256s-23.73 90.03-63.48 115.42c-11.19 7.14-14.5 22.07-7.41 33.36 6.51 10.36 21.12 15.14 33.12 7.46C447.94 377.94 480 319.54 480 256zm-141.77-76.87c-11.58-6.33-26.19-2.16-32.61 9.45-6.39 11.61-2.16 26.2 9.45 32.61C327.98 228.28 336 241.63 336 256c0 14.38-8.02 27.72-20.92 34.81-11.61 6.41-15.84 21-9.45 32.61 6.43 11.66 21.05 15.8 32.61 9.45 28.23-15.55 45.77-45 45.77-76.88s-17.54-61.32-45.78-76.86z\"></path></svg>",
			color: "info",
			text: "Set volume to {value}%"
		}
	},
	dmx: {
		set: {
			icon: "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"lightbulb\" class=\"svg-inline--fa fa-lightbulb fa-w-11\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 352 512\"><path fill=\"currentColor\" d=\"M96.06 454.35c.01 6.29 1.87 12.45 5.36 17.69l17.09 25.69a31.99 31.99 0 0 0 26.64 14.28h61.71a31.99 31.99 0 0 0 26.64-14.28l17.09-25.69a31.989 31.989 0 0 0 5.36-17.69l.04-38.35H96.01l.05 38.35zM0 176c0 44.37 16.45 84.85 43.56 115.78 16.52 18.85 42.36 58.23 52.21 91.45.04.26.07.52.11.78h160.24c.04-.26.07-.51.11-.78 9.85-33.22 35.69-72.6 52.21-91.45C335.55 260.85 352 220.37 352 176 352 78.61 272.91-.3 175.45 0 73.44.31 0 82.97 0 176zm176-80c-44.11 0-80 35.89-80 80 0 8.84-7.16 16-16 16s-16-7.16-16-16c0-61.76 50.24-112 112-112 8.84 0 16 7.16 16 16s-7.16 16-16 16z\"></path></svg>",
			color: "warning",
			text: "DMX channel {address} value {value}"
		}
	}
};
class PSC extends HTMLElement {
	constructor() {
		super();
		this.json = JSON.parse(this.innerText);
		this.shadow = this.attachShadow({mode: "open"});
		this.moduleName = this.getAttribute("module");
		this.command = this.getAttribute("command");
		const clone = PSCTemplate.content.cloneNode(true);
		let text = modules[this.moduleName][this.command].text;
		for (let prop in this.json) {
			if (this.json.hasOwnProperty(prop)) {
				text = text.replace(new RegExp(`{${prop}}`), this.json[prop]);
			}
		}
		let tag = document.createElement("div");
		tag.classList.add("tag");
		tag.classList.add("is-" + modules[this.moduleName][this.command].color);
		clone.append(tag);
		let divMrdka = document.createElement("div");
		divMrdka.innerHTML = modules[this.moduleName][this.command].icon;
		tag.append(divMrdka.firstChild);
		tag.append(document.createTextNode(text));
		this.shadow.append(clone);

	}

	static get observedAttributes() {
		return ["module", "command"];
	}

	/*attributeChangedCallback(name, oldValue, newValue) {

	}*/
}
customElements.define("psc-action", PSC);

class PSCBlock extends HTMLElement {
	constructor() {
		super();
		this.shadow = this.attachShadow({mode: "open"});
		const clone = PSCBlockTemplate.content.cloneNode(true);
		clone.getElementById("clicker").addEventListener("click", () => {
			this.execute();
		});
		this.addEventListener("run", () => {
			this.execute();
		});
		let innerText = [];
		for (let i = 0; i < this.constructor.observedAttributes.length; i++) {
			if (this.getAttribute(this.constructor.observedAttributes[i]) !== null) {
				innerText.push(this.constructor.observedAttributes[i] + ": " + this.getAttribute(this.constructor.observedAttributes[i]))
			}
		}
		clone.getElementById("params").innerText = innerText.join(" | ");
		this.shadow.append(clone);
	}
	execute() {
		this.callChildren()
	}
	callChildren() {
		[...this.shadow.getElementById("buttons").assignedElements({flatten: true}).filter(e => e.tagName.indexOf("PSC") === 0)].forEach(e => e.click());
		[...this.shadow.getElementById("buttons").assignedElements({flatten: true}).filter(e => e.tagName.indexOf("PSC") === 0)].forEach(e => e.dispatchEvent(new Event("run")));
	}
	static get observedAttributes() {
		return ["id"]
	}
}
customElements.define("psc-block", PSCBlock);

class PSCTimeout extends PSCBlock {
	constructor() {
		super();
		this.shadow.getElementById("wrapper").style.borderColor = "#0070ff";
		this.shadow.getElementById("wrapper").style.backgroundColor = "#00b7ff";
		this.shadow.getElementById("clicker").style.backgroundColor = "#00b7ff66";
	}
	execute() {
		if (typeof this.timeout === "number") {
			clearTimeout(this.timeout);
			this.timeout = null;
			this.shadow.getElementById("running").style.display = "none";
		} else {
			this.shadow.getElementById("running").style.display = "inline";
			this.timeout = setTimeout(() => {
				this.shadow.getElementById("running").style.display = "none";
				this.timeout = null;
				this.callChildren();
			}, parseInt(this.getAttribute("timeout")));
			if (typeof window.PSCTimeouts === "undefined") {
				window.PSCTimeouts = [];
			}
			window.PSCTimeouts.push(this.timeout);
		}
	}
	static get observedAttributes() {
		return ["id","timeout"]
	}
	static clearAllTimeouts() {
		window.PSCTimeouts.forEach(e => clearTimeout(e));
	}

}
customElements.define("psc-timeout", PSCTimeout);

class PSCInterval extends PSCBlock {
	constructor() {
		super();
		this.shadow.getElementById("wrapper").style.borderColor = "#009e20";
		this.shadow.getElementById("wrapper").style.backgroundColor = "#00DF2B";
		this.shadow.getElementById("clicker").style.backgroundColor = "#00DF2B66";
	}
	execute() {
		if (typeof this.interval === "number") {
			clearInterval(this.interval);
			this.interval = null;
			this.shadow.getElementById("running").style.display = "none";
		} else {
			this.shadow.getElementById("running").style.display = "inline";
			this.interval = setInterval(() => {
				this.callChildren();
			}, parseInt(this.getAttribute("interval")));
			if (typeof window.PSCIntervals === "undefined") {
				window.PSCIntervals = [];
			}
			window.PSCIntervals.push(this.interval);
		}
	}
	static get observedAttributes() {
		return ["id","interval"]
	}
	static clearAllIntervals() {
		window.PSCIntervals.forEach(e => clearInterval(e));
	}

}
customElements.define("psc-interval", PSCInterval);

class PSCCall extends HTMLElement {
	constructor() {
		super();
		this.shadow = this.attachShadow({mode: "open"});
		const clone = PSCCallTemplate.content.cloneNode(true);
		this.target = this.getAttribute("block-id");

		if (this.getAttribute("clear") !== null) {
			clone.getElementById("tag").innerText = `Cancel all ${this.getAttribute("clear")}`;
		} else {
			clone.getElementById("tag").innerText = `Call block "${this.target}"`;
		}


		this.addEventListener("click", () => {
			if (this.getAttribute("clear") !== null) {
				if (this.getAttribute("clear") === "intervals") {
					PSCInterval.clearAllIntervals();
				} else if (this.getAttribute("clear") === "timeouts") {
					PSCTimeout.clearAllTimeouts();
				}
			} else {
				document.getElementById(this.target).dispatchEvent(new Event("run"));
			}
		});


		this.shadow.append(clone);
	}
	static get observedAttributes() {
		return ["block-id"]
	}
}
customElements.define("psc-call", PSCCall);

