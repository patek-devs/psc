import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import './assets/scss/app.scss'
import router from './router'

Vue.use(Buefy);

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || "PátekSC"
  next()
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
