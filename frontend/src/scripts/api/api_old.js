const api_old = class Api {
	constructor(url) {
		this.url = url;
	}
	createProject(slug) {
		return new Promise(resolve => {
			fetch(this.url + "/projects/" + slug, {
				method: "PATCH"
			}).then(() => {
				this.currentProject = new Project(slug, this);
				resolve();
			});
		});
	}
	openProject(slug) {
		this.currentProject = new Project(slug, this);
	}
	getProjects() {
		return new Promise(resolve => {
			fetch(this.url + "/projects").then(response => response.json()).then(json => {
				this.projects = json;
				resolve();
			});
		});
	}
}
class Project {
	constructor(slug, api) {
		this.slug = slug;
		this.api = api;
		this.url = this.api.url + "/projects/" + slug;
	}
	getSource() {
		return fetch(this.url + "/source");
	}
	putSource(source) {
		return fetch(this.url + "/source", {
			method: "PUT",
			body: source
		});
	}
	postFile(fileList, type) {
		if (fileList.length > 1) {
			throw new RangeError("Too much files. Pls only one. K thx bye.");
		}
		return fetch(this.url + "/files/" + type, {
			method: "POST",
			body: fileList[0]
		});
	}
	modules = {
		audio: {
			play: createCommand(this.url, "audio", "play"),
			stop: createCommand(this.url, "audio", "stop"),
			volume: createCommand(this.url, "audio", "volume"),
		},
		dmx: {
			set: createCommand(this.url, "dmx", "set"),
		}
	};

}
function createCommand(projecturl, moduleName, command) {
	return (params) => {
		return fetch(projecturl + "/exec/" + moduleName + "/" + command, {
			method: "POST",
			body: params
		});
	}
}

export default api_old