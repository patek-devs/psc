import api from "./api.js"

const resource = "/projects"
var project = ""

function createCommand(projecturl, moduleName, command) {
    return (params) => {
        return fetch(projecturl + "/exec/" + moduleName + "/" + command, {
            method: "POST",
            body: params
        });
    }
}

export default {
    api,
    createProject(slug) {
        return api.put(`${resource}/${slug}`)
    },
    openProject(slug) {
        project = slug
    },
    getProjects() {
        return api.get(resource)
    },
    getSource() {
        return api.get(resource+"/"+project+'/source')
    },
    putSource(source) {
        return api.post(`${resource}/${project}/source`, source)
    },
    postFile(fileList, type) {
        if (fileList.length > 1) {
            throw new RangeError("Too much files. Pls only one. K thx bye.");
        }
        return api.post( '/files/'+type, fileList, {headers: {'Content-Type': 'multipart/form-data'}})
    },
    modules: {
        audio: {
            play: createCommand(`${resource}`, "audio", "play"),
            stop: createCommand(`${resource}`, "audio", "stop"),
            volume: createCommand(`${resource}`, "audio", "volume"),
        },
        dmx: {
            set: createCommand(`${resource}`, "dmx", "set"),
        }
    },

}