const axios = require("axios")

const url = "http://pscpi:8080"

export default axios.create({
    baseURL: url
})
