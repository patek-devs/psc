import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home.vue'
import Script from '../views/script.vue'
import Buttons from '../views/buttons.vue'
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    meta: { title: 'Home - PátekSC' },
    component: Home
  },
  {
    path: '/script',
    name: 'script',
    meta: { title: 'Script - PátekSC' },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Script
  },
  {
    path: '/buttons',
    name: 'buttons',
    meta: { title: 'Manual Control - PátekSC' },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Buttons
  }
]

const router = new VueRouter({
  routes
})

export default router
