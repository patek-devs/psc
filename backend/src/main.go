package main

import (
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/faiface/beep"
	"github.com/faiface/beep/effects"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/beep/wav"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/tarm/serial"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

const projectsPath = "/psc/projects"
const speakerSampleRate beep.SampleRate = 40000 //Hz
const speakerBuffer = time.Millisecond * 150
const dmxRequestsBuffer = 1000

var allowedTypes = map[string]bool{"audio": true}
var contentTypesToExtensions = map[string]string{
	"image/png": "png",
	"image/jpg": "jpg",
	"audio/mp3": "mp3",
	"audio/wav": "wav",
}

type audioPlayRequest struct {
	File string `json:"file"`
}

type dmxOutputRequest struct {
	address uint16
	value   byte
}

type dmxInputRequest struct {
	Address json.Number `json:"address"`
	Value   json.Number `json:"value"`
}

func (d *dmxInputRequest) toDmxOutputRequest() (dmxOutputRequest, error) {
	var or dmxOutputRequest
	if a, err := d.Address.Int64(); err == nil {
		or.address = uint16(a)
	} else {
		return or, err
	}
	if v, err := d.Value.Int64(); err == nil {
		or.value = byte(v)
	} else {
		return or, err
	}
	return or, nil
}

func contains(arr []string, str string) int {
   for i, a := range arr {
      if a == str {
         return i
      }
   }
   return -1
}

const banner = `

                               j$W @w@,,                    
      ,,,             ,,,      j$$$$$$$$$@@w                
  z $$$$$$$@@     a $$$$$$$Wm      `+"`"+`^"M$$$$$$@@             
 $$$$^    ^"^   ,$$$M^   `+"`"+`"^             `+"`"+`"$$$$$W,          
 $$$$Ww,        $$$[                         "$$$$W         
  "$$$$$$$ @   j$$$                            "$$$$@       
      ^" $$$$U )$$$W                            `+"`"+`$$$$       
         j$$$[  $$$$U                             $$$$      
.$$@@w@@@$$$M    \$$$@@ww $$U   a,                 $$$$U    
  ^" WWWWM"^       ^" WWW "^    =w^W               ]$$$@    
                                  ^U$               $$$$    
       iWWW[               j       [}U              $$$$    
       ]$$$@              "@"v,,,w^/^              j$$$$    
        $$$$                ^"WwWM^                 $$$     
        ]$$$@                                     i$$$$     
          $$$@                                   z$$$$      
          $$$$@U                               ,@$$$$       
           "$$$$W,                           ,a$$$$W        
             \$$$$@@                       ,@$$$$M^         
               "$$$$$@@,,             ,,w $$$$$W^           
                 ^"$$$$$$$$W @wwwW@ @$$$$$$$ ^              
                     ^" $$$$$$$$$$$$$$$W"^                  
                            `+"`"+`^^^^^^                 
                          
             Welcome to the Pátek ShowController!
             
`

// Check whether the project exists. While it may disappear later and therefore you have to expect file accesses to fail,
// it is useful for users to report whether the project is simply misspelled or whether something more serious has happened
func checkProjectExists(project string) (bool, error) {
	if _, err := os.Stat(filepath.Join(projectsPath, project)); err == nil {
		return true, nil
	} else if os.IsNotExist(err) {
		return false, nil
	} else {
		return false, err
	}
}

func percentageToVolume(percentage float64) float64 {
	var volume = percentage/10 - 10
	if volume < -10 {
		volume = -10
	} else if volume > 2 {
		volume = 2
	}
	return volume
}

func main() {
	println(banner)
	if err := os.MkdirAll(projectsPath, 0770); err != nil {
		panic(err)
	}

	router := mux.NewRouter()

	router.HandleFunc("/projects", func(w http.ResponseWriter, r *http.Request) {
		if entries, err := ioutil.ReadDir(projectsPath); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			var buffer = make([]string, 0, len(entries))
			jsonEncoder := json.NewEncoder(w)
			jsonEncoder.SetEscapeHTML(false)
			for i := 0; i < len(entries); i++ {
				if entries[i].IsDir() {
					buffer = append(buffer, filepath.Base(entries[i].Name()))
				}
			}
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			jsonEncoder.Encode(buffer)
		}
	}).Methods("GET")

	router.HandleFunc("/projects/{project}", func(w http.ResponseWriter, r *http.Request) {
		var project = mux.Vars(r)["project"]
		if strings.ContainsRune(project, 0) || strings.ContainsRune(project, '/') {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if err := os.Mkdir(filepath.Join(projectsPath, project), 0770); err == nil {
			w.WriteHeader(http.StatusCreated)
			return
		} else if os.IsExist(err) {
			w.WriteHeader(http.StatusNoContent)
			return
		} else {
			fmt.Printf("%#v\n", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}).Methods("PUT")

	router.HandleFunc("/projects/{project}/source", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var project = vars["project"]
		if res, err := checkProjectExists(project); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else if !res {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Requested project was not found"))
			return
		}

		if file, err := os.Open(filepath.Join(projectsPath, project, "source")); os.IsNotExist(err) {
			w.WriteHeader(http.StatusNoContent)
			//w.WriteHeader(http.StatusNotFound)
			return
		} else if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			defer file.Close()
			w.Header().Set("Content-Type", "text/markdown; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			io.Copy(w, file)
			return
		}
	}).Methods("GET")

	router.HandleFunc("/projects/{project}/source", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var project = vars["project"]
		if res, err := checkProjectExists(project); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else if !res {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Requested project was not found"))
			w.WriteHeader(http.StatusNoContent)
			return
		}

		if file, err := os.Create(filepath.Join(projectsPath, project, "source")); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			defer file.Close()
			if _, err := io.Copy(file, r.Body); err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.WriteHeader(http.StatusCreated)
			return
		}
	}).Methods("POST")

	router.HandleFunc("/projects/{project}/files/{type}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		//cannot use `type`, it is a keyword
		var project, t = vars["project"], vars["type"]

		if !allowedTypes[t] {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Requested file type does not exist"))
			return
		}

		var extension = contentTypesToExtensions[r.Header.Get("Content-Type")]
		if ext, ok := contentTypesToExtensions[r.Header.Get("Content-Type")]; ok {
			extension = ext
		} else {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusUnsupportedMediaType)
			w.Write([]byte("Requested content-type unsupported"))
			return
		}

		if res, err := checkProjectExists(project); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else if !res {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Requested project was not found"))
			return
		}

		file, err := ioutil.TempFile("/psc", "")
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		defer file.Close()
		var tempName = file.Name()
		defer os.Remove(tempName) //The file should be already renamed, unless an error occurs

		hash := sha1.New()
		mw := io.MultiWriter(hash, file)
		if _, err := io.Copy(mw, r.Body); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		var id = base64.URLEncoding.EncodeToString(hash.Sum(nil))
		if err := os.Rename(tempName, filepath.Join(projectsPath, project, id+"."+extension)); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if err := file.Sync(); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "text/plain")
		w.WriteHeader(http.StatusCreated)
		fmt.Fprintf(w, "%s.%s", id, extension)
		return
	}).Methods("POST")

	if err := speaker.Init(speakerSampleRate, speakerSampleRate.N(speakerBuffer)); err != nil {
		panic(err)
	}
	var pscMix = &beep.Mixer{}
	var volume = &effects.Volume{
		Streamer: pscMix,
		Base:     1.73,
	}
	speaker.Play(volume)
	router.HandleFunc("/projects/{project}/exec/audio/play", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var project = vars["project"]
		if res, err := checkProjectExists(project); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else if !res {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Requested project was not found"))
			return
		}

		var req audioPlayRequest
		jsonDecoder := json.NewDecoder(r.Body)
		if err := jsonDecoder.Decode(&req); err != nil || strings.ContainsRune(req.File, '/') || strings.ContainsRune(req.File, 0) {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		file, err := os.Open(filepath.Join(projectsPath, project, req.File))
		if os.IsNotExist(err) {
			w.WriteHeader(http.StatusNotFound)
			return
		} else if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		streamer, format, err := wav.Decode(file) //TODO
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		//defer streamer.Close() //TODO resource leak
		pscMix.Add(beep.Resample(3, format.SampleRate, speakerSampleRate, streamer))
	})

	router.HandleFunc("/projects/{project}/exec/audio/stop", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var project = vars["project"]
		if res, err := checkProjectExists(project); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else if !res {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Requested project was not found"))
			return
		}

		pscMix.Clear()
		w.WriteHeader(http.StatusNoContent)
	}).Methods("POST")

	var serialConfig = serial.Config{
		Name: "/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0",
		Baud: 115200,
	}
	serialPort, err := serial.OpenPort(&serialConfig)
	if err != nil && contains(os.Args, "--ignore-serial") == -1 {
		panic(err)
	}
	var dmxRequests = make(chan dmxOutputRequest, dmxRequestsBuffer)

	go func() {
		for req := range dmxRequests {
			fmt.Fprintf(serialPort, "%dc%dw", req.address, req.value)
		}
	}()

	router.HandleFunc("/projects/{project}/exec/dmx/set", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var project = vars["project"]
		if res, err := checkProjectExists(project); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else if !res {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Requested project was not found"))
			return
		}

		var reqI dmxInputRequest
		jsonDecoder := json.NewDecoder(r.Body)
		if err := jsonDecoder.Decode(&reqI); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if reqO, err := reqI.toDmxOutputRequest(); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		} else {
			if contains(os.Args, "--ignore-serial") == -1 {
				dmxRequests <- reqO
			}
			w.WriteHeader(http.StatusNoContent)
			return
		}
	})

	router.HandleFunc("/projects/{project}/exec/volume/set/{volume}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		var project = vars["project"]
		var volumeString = vars["volume"]
		if res, err := checkProjectExists(project); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else if !res {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Requested project was not found"))
			return
		}

		if v, err := strconv.ParseFloat(volumeString, 64); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		} else {
			volume.Volume = percentageToVolume(v)
			w.WriteHeader(http.StatusNoContent)
			return
		}
	}).Methods("POST")

	http.Handle("/", handlers.CORS()(router))
	port := ":8080"
	if (contains(os.Args, "--port") != -1) {
		port = ":" + os.Args[contains(os.Args, "--port") + 1]
	}
	log.Fatal(http.ListenAndServe(port, handlers.CORS(handlers.AllowedHeaders([]string{"X-Requested-With","content-type"}), handlers.AllowedOrigins([]string{"*"}), handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"}))(router)))

}
